﻿using System;
using System.IO;
using System.Text;

namespace RelaxationMethod
{
    class Program
    {
        const double omega = 0.5; // 0.0 < omega < 2.0
        const double eps = 0.0001;
        const int MAX_STEPS = 1000;
        static double[] xk; // start point

        static readonly string outfile = "output.csv";

        static void Main(string[] args)
        {            
            // Start SLAY
            double[,] A;
            int N;

            double[] b;

            // Get Input data

            do { Console.Write("N:N = "); }
            while (!int.TryParse(Console.ReadLine(), out N) || N <= 0);

            A = new double[N, N];
            b = new double[N];

            for (int r = 0; r < N; ++r)
            {
                for (int col = 0; col < N; ++col)
                {
                    do { Console.Write("m[{0}, {1}] = ", r + 1, col + 1); }
                    while (!double.TryParse(Console.ReadLine(), out A[r, col]));
                }
            }

            for (int i = 0; i < N; ++i)
            {
                do { Console.Write("b[|{0}|] = ", i); }
                while (!double.TryParse(Console.ReadLine(), out b[i]));
            }

            xk = new double[N];

            using (var writer = new StreamWriter(outfile, false, Encoding.Default))
            {
                var sb = new StringBuilder("N;");
                
                for (int i = 0; i < N; ++i)
                {
                    sb.Append(string.Format("X{0};R{0};", i + 1));
                }

                writer.WriteLine(sb.ToString());
            }           

            // New SLAY
            double[,] B = new double[N, N];
            double[] c = new double[N];

            double[] xk1 = new double[N]; // x^{(k+1)}
            int step = 0;
            double norm2;

            // Calculate new SLAY
            for (int i = 0; i < N; ++i)
            {
                c[i] = b[i] / A[i, i];
                for (int j = 0; j < N; ++j)
                {
                    B[i, j] = -A[i, j] / A[i, i];
                }
            }


            do
            {
                // Next point
                for (int i = 0; i < N; ++i)
                {
                    xk1[i] = (1.0 - omega) * xk[i];
                    double s = c[i];
                    for (int j = 0; j < i; ++j)
                    {
                        s += B[i, j] * xk1[j];
                    }
                    for (int j = i + 1; j < N; ++j)
                    {
                        s += B[i, j] * xk[j];
                    }
                    xk1[i] += omega * s;
                }

                // вычисление нормы разности в квадрате
                norm2 = 0.0;
                for (int i = 0; i < N; ++i)
                {
                    norm2 += (xk[i] - xk1[i]) * (xk[i] - xk1[i]);
                }

                // Сохраняем x^{k+1} в x^{k}
                for (int i = 0; i < N; ++i)
                {
                    xk[i] = xk1[i];
                }

                // Печатаем информацию о текущем шаге                

                ++step;

                using var writer = new StreamWriter(outfile, true, Encoding.Default);
                var sb = new StringBuilder($"{step};");

                Console.WriteLine("step: {0}", step);
                Console.WriteLine("  norm^2: {0}", norm2);
                Console.Write("  x:");
                for (int i = 0; i < N; ++i)
                {
                    Console.Write(" {0}", xk[i]);
                }
                Console.WriteLine();
                Console.Write("  A*x: ");
                double[] r = new double[N];
                for (int i = 0; i < N; ++i)
                {
                    r[i] = 0.0;
                    for (int j = 0; j < N; ++j)
                    {
                        r[i] += A[i, j] * xk[j];
                    }
                    Console.Write(" {0}", r[i]);
                }

                for (int i = 0; i < N; ++i)
                {
                    sb.Append(string.Format("{0:F4};{1:F4};", xk[i], r[i]));
                }

                writer.WriteLine(sb.ToString());

                Console.WriteLine();


            } while ((norm2 >= eps * eps) && (step < MAX_STEPS));
        }

        
    }
}
